-- ------------------------------------------------------
-- VB version 2.4.1 SQL Script
-- ------------------------------------------------------

-- Add (Move,link,unlink,addtoscheme,removefromscheme)permission to Ontology Editor
INSERT IGNORE INTO permission_functionality_map VALUES ('76', '4', '1')  , ('76', '4', '2') , ('76', '4', '3')  , ('76', '4', '4')  , ('76', '4', '5')  , ('76', '4', '7')  , ('76', '4', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('77', '4', '1')  , ('77', '4', '2') , ('77', '4', '3')  , ('77', '4', '4')  , ('77', '4', '5')  , ('77', '4', '7')  , ('77', '4', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('78', '4', '1')  , ('78', '4', '2') , ('78', '4', '3')  , ('78', '4', '4')  , ('78', '4', '5')  , ('78', '4', '7')  , ('78', '4', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('82', '4', '1')  , ('82', '4', '2') , ('82', '4', '3')  , ('82', '4', '4')  , ('82', '4', '5')  , ('82', '4', '7')  , ('82', '4', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('83', '4', '1')  , ('83', '4', '2') , ('83', '4', '3')  , ('83', '4', '4')  , ('83', '4', '5')  , ('83', '4', '7')  , ('83', '4', '8') ;