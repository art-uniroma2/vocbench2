-- ------------------------------------------------------
-- VB version 2.4.1 SQL Script
-- ------------------------------------------------------

-- Add validation permission for project manager
INSERT IGNORE INTO `validation_permission` (`users_groups_id`, `action`, `status`, `newstatus`)  
SELECT users_groups_id, 1, 1, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 2, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 3, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 4, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 5, 6 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 6, 0 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 7, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 8, 0 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 1, 9, 0 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 1, 99 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 2, 99 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 3, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 4, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 5, 8 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 6, 0 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 7, 99 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 8, 0 FROM users_groups WHERE users_groups_name='Project manager' UNION ALL 
SELECT users_groups_id, 2, 9, 0 FROM users_groups WHERE users_groups_name='Project manager';

-- Add status action for Project manager
INSERT IGNORE INTO status_action_map SELECT '1',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '2',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '3',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '4',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '5',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '6',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '7',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '8',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '9',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '10',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '11',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '12',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '13',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '14',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '15',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '16',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '17',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '18',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '19',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '20',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '21',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '22',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '23',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '24',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '25',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '26',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '27',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '28',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '29',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '30',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '31',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '32',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '33',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '34',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '35',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '36',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '37',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '38',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '39',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '40',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '41',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '42',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '76',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '77',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO status_action_map SELECT '78',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';

-- Add new actions
INSERT IGNORE INTO `owl_action` (`id`, `action`, `action_child`) VALUES 
(82, 'concept-edit', 'add-to-scheme'), 
(83, 'concept-edit', 'remove-from-scheme'),
(84, 'concept-edit', 'resource-view'),
(85, 'search-index', ''),
(86, 'scheme-edit', ''),
(87, 'scheme-set-default', ''),
(88, 'project-showall', ''),
(89, 'graph-visualization', '');


-- Add (Move,link,unlink,addtoscheme,removefromscheme)permission to Administrator
INSERT IGNORE INTO permission_functionality_map VALUES ('76', '1', '1')  , ('76', '1', '2') , ('76', '1', '3')  , ('76', '1', '4')  , ('76', '1', '5')  , ('76', '1', '7')  , ('76', '1', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('77', '1', '1')  , ('77', '1', '2') , ('77', '1', '3')  , ('77', '1', '4')  , ('77', '1', '5')  , ('77', '1', '7')  , ('77', '1', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('78', '1', '1')  , ('78', '1', '2') , ('78', '1', '3')  , ('78', '1', '4')  , ('78', '1', '5')  , ('78', '1', '7')  , ('78', '1', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('82', '1', '1')  , ('82', '1', '2') , ('82', '1', '3')  , ('82', '1', '4')  , ('82', '1', '5')  , ('82', '1', '7')  , ('82', '1', '8') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('83', '1', '1')  , ('83', '1', '2') , ('83', '1', '3')  , ('83', '1', '4')  , ('83', '1', '5')  , ('83', '1', '7')  , ('83', '1', '8') ;
 

-- Add (Move,link,unlink,addtoscheme,removefromscheme)permission to Project Manager
INSERT IGNORE INTO permission_functionality_map SELECT '76',users_groups_id, '1' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '76',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '76',users_groups_id, '3' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '76',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '76',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '76',users_groups_id, '7' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '76',users_groups_id, '8' FROM users_groups WHERE users_groups_name='Project manager';

INSERT IGNORE INTO permission_functionality_map SELECT '77',users_groups_id, '1' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '77',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '77',users_groups_id, '3' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '77',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '77',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '77',users_groups_id, '7' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '77',users_groups_id, '8' FROM users_groups WHERE users_groups_name='Project manager';

INSERT IGNORE INTO permission_functionality_map SELECT '78',users_groups_id, '1' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '78',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '78',users_groups_id, '3' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '78',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '78',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '78',users_groups_id, '7' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '78',users_groups_id, '8' FROM users_groups WHERE users_groups_name='Project manager';

INSERT IGNORE INTO permission_functionality_map SELECT '82',users_groups_id, '1' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '82',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '82',users_groups_id, '3' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '82',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '82',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '82',users_groups_id, '7' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '82',users_groups_id, '8' FROM users_groups WHERE users_groups_name='Project manager';

INSERT IGNORE INTO permission_functionality_map SELECT '83',users_groups_id, '1' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '83',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '83',users_groups_id, '3' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '83',users_groups_id, '4' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '83',users_groups_id, '5' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '83',users_groups_id, '7' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '83',users_groups_id, '8' FROM users_groups WHERE users_groups_name='Project manager';

-- Add following permission ('search-index', 'scheme-add', 'scheme-delete', 'scheme-edit',  'scheme-set-default',  'project-showall') to Administrator & Project manager
INSERT IGNORE INTO permission_functionality_map VALUES ('85', '1', '2') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('40', '1', '2') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('75', '1', '2') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('86', '1', '2') ;
INSERT IGNORE INTO permission_functionality_map VALUES ('87', '1', '2') ;
INSERT IGNORE INTO permission_functionality_map SELECT '40',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '75',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '86',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';
INSERT IGNORE INTO permission_functionality_map SELECT '87',users_groups_id, '2' FROM users_groups WHERE users_groups_name='Project manager';



INSERT IGNORE INTO permission_functionality_map VALUES ('88', '1', '2') ;

-- Add graph visualization to all user group 
INSERT IGNORE INTO permission_functionality_map SELECT '89',users_groups_id, '2' FROM users_groups 



