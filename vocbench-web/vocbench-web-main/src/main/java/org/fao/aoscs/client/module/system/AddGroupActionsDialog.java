package org.fao.aoscs.client.module.system;

import java.util.ArrayList;
import java.util.Iterator;

import org.fao.aoscs.client.Service;
import org.fao.aoscs.client.locale.LocaleConstants;
import org.fao.aoscs.client.utility.ExceptionManager;
import org.fao.aoscs.client.widgetlib.shared.dialog.FlexDialogBox;
import org.fao.aoscs.domain.OwlAction;
import org.fao.aoscs.domain.OwlStatus;
import org.fao.aoscs.domain.PermissionFunctionalityMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class AddGroupActionsDialog extends FlexDialogBox implements ClickHandler {
	private static LocaleConstants constants = (LocaleConstants) GWT.create(LocaleConstants.class);
	
	private VerticalPanel container = new VerticalPanel();
	
	
	private ListBox actionList = new ListBox();
	private ListBox statusList = new ListBox();
	private VerticalPanel permission_statusList = new VerticalPanel();
	private String groupId;
	
	public AddGroupActionsDialog(String gid) {
		super(constants.buttonAdd(), constants.buttonCancel());
		this.groupId = gid;
		setText(constants.groupAddAction());
		center();
		actionList.setMultipleSelect(true);
		actionList.setVisibleItemCount(16);
		
		AsyncCallback<ArrayList<OwlStatus>> callback = new AsyncCallback<ArrayList<OwlStatus>>() {	
			public void onSuccess(ArrayList<OwlStatus> tmp) {
				statusList.clear();
				statusList.addItem("--"+constants.buttonSelect()+"--", "-1");
				for(int i=0;i<tmp.size();i++){
					OwlStatus os = (OwlStatus)tmp.get(i);
					statusList.addItem(os.getStatus(), ""+os.getId());
					
					CheckBox cb = new CheckBox(os.getStatus());
					cb.setFormValue(""+os.getId());
					permission_statusList.add(cb);
				}
				initActionList(groupId);
			}
			public void onFailure(Throwable caught) {
				ExceptionManager.showException(caught, constants.groupUserListFail());
			}
		};
		Service.validationService.getStatus(callback);
		
		// create UI for action-status input
		container.add(createStatusInput(constants.groupActions()+":&nbsp;", actionList));
		container.add(createStatusInput(constants.groupAddActionStatus()+":&nbsp;", statusList));
		container.add(permission_statusList);

		container.setSpacing(5);
		container.setWidth("100%");
		
		addWidget(container);
		
	}
	
	private void initActionList(final String groupId){
		AsyncCallback<ArrayList<OwlAction>> callback = new AsyncCallback<ArrayList<OwlAction>>() {	
			public void onSuccess(ArrayList<OwlAction> tmp) {
			    Iterator<OwlAction> it = tmp.iterator();
			    actionList.clear();
				while(it.hasNext()){
					OwlAction oa = (OwlAction)it.next();
					String label = oa.getAction() + (oa.getActionChild().length()>0? "-"+oa.getActionChild() : "");
					actionList.addItem(label, ""+oa.getId());
				}
			}

			public void onFailure(Throwable caught) {
				ExceptionManager.showException(caught, constants.groupUserListFail());
			}
		};
		
		Service.systemService.getUnassignedActions(groupId, callback);
	}
	
	private Widget createStatusInput(String label, Widget w){
		HorizontalPanel addActionContainer = new HorizontalPanel();
		addActionContainer.add(new HTML("<b>"+label+"</b>"));
		addActionContainer.add(w);
		return addActionContainer;
	}
	
	public boolean passCheckInput(){
		boolean ret = false;
		int wCount = permission_statusList.getWidgetCount();
		for(int i=0; i<wCount; i++){
			Widget w = permission_statusList.getWidget(i);
			if(w instanceof CheckBox)
			{
				CheckBox cb = (CheckBox) w;
				if(cb.getValue())
					ret = true;
			}
		}
		
		if(actionList.getSelectedIndex() > 0 && (ret || statusList.getSelectedIndex() > 0))
			ret = true;
		else 
			ret = false;
		return ret;
	}
	
	public void onSubmit(){
		int wCount = permission_statusList.getWidgetCount();
		ArrayList<PermissionFunctionalityMap> map = new ArrayList<PermissionFunctionalityMap>();
		ArrayList<PermissionFunctionalityMap> actionStatusToGroup = new ArrayList<PermissionFunctionalityMap>();
		
		
		for(int a=0;a<actionList.getItemCount();a++)
		{
			if(actionList.isItemSelected(a))
			{
				String actionId = actionList.getValue(a);
				for(int i=0; i<wCount; i++){
					Widget w = permission_statusList.getWidget(i);
					if(w instanceof CheckBox)
					{
						CheckBox cb = (CheckBox) w;
						if(cb.getValue()){
							PermissionFunctionalityMap item = new PermissionFunctionalityMap();
							item.setFunctionId(Integer.parseInt(actionId));
							item.setGroupId(Integer.parseInt(this.groupId));
							item.setStatus(Integer.parseInt(cb.getFormValue()));
							map.add(item);
						}
					}
				}
				
				PermissionFunctionalityMap pfm = new PermissionFunctionalityMap();
				pfm.setFunctionId(Integer.parseInt(actionId));
				pfm.setGroupId(Integer.parseInt(this.groupId));
				pfm.setStatus(Integer.parseInt(statusList.getValue(statusList.getSelectedIndex())));
				actionStatusToGroup.add(pfm);
			}
		}
		
		AsyncCallback<Void> callback = new AsyncCallback<Void>(){	
			public void onSuccess(Void result){
				hide();
				handlerManager.fireEvent(new FlexDialogSubmitClickedEvent(null));
			}
			public void onFailure(Throwable caught) {
				ExceptionManager.showException(caught, constants.groupUserListFail());
			}
		};
		Service.systemService.addActionToGroup(map, actionStatusToGroup,  callback);
	}
	
}