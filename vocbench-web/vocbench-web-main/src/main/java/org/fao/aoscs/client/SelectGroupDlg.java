package org.fao.aoscs.client;

import java.util.ArrayList;

import org.fao.aoscs.client.locale.LocaleConstants;
import org.fao.aoscs.client.module.logging.LogManager;
import org.fao.aoscs.client.widgetlib.shared.dialog.DialogBoxAOS;
import org.fao.aoscs.client.widgetlib.shared.misc.OlistBox;
import org.fao.aoscs.client.widgetlib.shared.panel.TitleBodyWidget;
import org.fao.aoscs.domain.OntologyInfo;
import org.fao.aoscs.domain.UserLogin;
import org.fao.aoscs.domain.UsersGroups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SelectGroupDlg extends DialogBoxAOS{
	
	private LocaleConstants constants = (LocaleConstants) GWT.create(LocaleConstants.class);

	private OlistBox lstgroups = new OlistBox();
	private TextArea txtgroups = new TextArea();
	private Button btnSubmit = new Button(constants.buttonSubmit());
	private Button btnCancel = new Button(constants.buttonCancel());
	
	private UserLogin userLoginObj;
	
	public SelectGroupDlg(UserLogin  userLoginObj, OntologyInfo ontoInfo, ArrayList<UsersGroups> listgroups) {				
		this.userLoginObj = userLoginObj;
		initPanels(ontoInfo);
		load(listgroups);
	}
	
	public void initPanels(final OntologyInfo ontoInfo){
		// popup element
		this.setText(constants.userGroup());
		
		lstgroups.setWidth("200px");
		lstgroups.setVisibleItemCount(6);
		lstgroups.getElement().getStyle().setCursor(Cursor.POINTER);
		
		txtgroups.setWidth("200px");
		txtgroups.setVisibleLines(4);
		txtgroups.setReadOnly(true);
		txtgroups.getElement().getStyle().setMarginTop(0, Unit.PX);
		
	    TitleBodyWidget tbwidgetgroup = new TitleBodyWidget(constants.selPrefGroup(), lstgroups, null, txtgroups,  "200px", "100%");
	    
		HorizontalPanel panel = new HorizontalPanel();
		panel.setSpacing(10);
		panel.setWidth("100%");
		panel.add(tbwidgetgroup);
				
		HorizontalPanel hp = new HorizontalPanel();
		hp.setSpacing(5);
		hp.add(btnSubmit);
		hp.add(btnCancel);
		
		VerticalPanel bottompanel = new VerticalPanel();
		bottompanel.setWidth("100%");
		bottompanel.addStyleName("bottombar");
		bottompanel.add(hp);
		bottompanel.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(panel);
		vp.add(bottompanel);
		
		vp.setCellHorizontalAlignment(hp,HasHorizontalAlignment.ALIGN_RIGHT);
		vp.setWidth("100%");
		setWidget(vp);
		btnCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Main.gotoLoginScreen();
				hide();
			}
		
		});

		btnSubmit.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				userLoginObj.setOntology(ontoInfo);
				userLoginObj.setGroupid(""+((UsersGroups) lstgroups.getObject(lstgroups.getSelectedIndex())).getUsersGroupsId());
				userLoginObj.setGroupname(""+((UsersGroups) lstgroups.getObject(lstgroups.getSelectedIndex())).getUsersGroupsName());
		    	MainApp mainApp = new MainApp(userLoginObj);
		    	new LogManager().startLog(""+userLoginObj.getUserid());
		    	mainApp.setWidth("100%");
				Main.replaceRootPanel(mainApp); 
			}
		});
	}
	
	public void load(ArrayList<UsersGroups> listgroups)
	{
		//Set user groups
		lstgroups.clear();
		for(int i=0;i<listgroups.size();i++){
    		UsersGroups userGroups = (UsersGroups) listgroups.get(i);
    		lstgroups.addItem(userGroups.getUsersGroupsName(),userGroups);
    	}
		
    	if(lstgroups.getItemCount()>0)
    		lstgroups.setItemSelected(0, true);
    	
    	String descGroups = ((UsersGroups) lstgroups.getObject(lstgroups.getSelectedIndex())).getUsersGroupsDesc();
	    lstgroups.setTitle(descGroups);
	    txtgroups.setText(descGroups);
	    lstgroups.addChangeHandler(new ChangeHandler(){
			public void onChange(ChangeEvent event) {
				String descGroups = ((UsersGroups) lstgroups.getObject(lstgroups.getSelectedIndex())).getUsersGroupsDesc();
				lstgroups.setTitle(descGroups);
				txtgroups.setText(descGroups);
			}
	    	
	    });
	    
	}

}
