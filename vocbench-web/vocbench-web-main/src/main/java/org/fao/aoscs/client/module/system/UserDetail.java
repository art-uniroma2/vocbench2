package org.fao.aoscs.client.module.system;

import java.util.ArrayList;
import java.util.HashMap;

import org.fao.aoscs.client.MainApp;
import org.fao.aoscs.client.Service;
import org.fao.aoscs.client.locale.LocaleConstants;
import org.fao.aoscs.client.module.preferences.service.UsersPreferenceService.UserPreferenceServiceUtil;
import org.fao.aoscs.client.utility.ExceptionManager;
import org.fao.aoscs.client.utility.GridStyle;
import org.fao.aoscs.client.widgetlib.shared.dialog.DialogBoxAOS;
import org.fao.aoscs.client.widgetlib.shared.panel.TitleBodyWidget;
import org.fao.aoscs.domain.Users;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class UserDetail extends Composite implements ClickHandler {

	private LocaleConstants constants = (LocaleConstants) GWT.create(LocaleConstants.class);

	private ListBox lstusergroups = new ListBox();
	private ListBox lstuserlangs = new ListBox();
	
	private Image btnaddlang = new Image("images/add-grey.gif");
	private Image btnremovelang = new Image("images/delete-grey.gif");
	private Image btnaddgroup = new Image("images/add-grey.gif");
	private Image btnremovegroup = new Image("images/delete-grey.gif");
	
	private UserAssignDialogBox newUserDialog;
	
	private FlexTable ft = new FlexTable();
	private TextBox txtloginname = new TextBox();
	private TextBox txtfname = new TextBox();
	private TextBox txtlname = new TextBox();
	private TextBox txtemail = new TextBox();
	private TextBox txtaddress = new TextBox();
	private TextBox txturl = new TextBox();
	private TextBox txtaffiliation = new TextBox();
	private TextBox txtcountry = new TextBox();
	private TextBox txtpostcode = new TextBox();
	private TextBox txtwphone = new TextBox();
	private TextBox txtmphone = new TextBox();
	private TextBox txtchat = new TextBox();
	private VerticalPanel userSubPanel = new VerticalPanel();
	private String userid = "";
	private String ontologyid = "";
	
	private VerticalPanel panel = new VerticalPanel();

	private HashMap<String, String> countryList = new HashMap<String, String>();

	public UserDetail() {		
		
		
		
		// GROUP ASSIGNMENT			
		lstusergroups.setVisibleItemCount(8);
		lstusergroups.setMultipleSelect(true);
		lstusergroups.setHeight("100px");
		
		btnaddgroup.setTitle(constants.buttonAdd());
		btnremovegroup.setTitle(constants.buttonRemove());
		btnaddgroup.addClickHandler(this);
		btnremovegroup.addClickHandler(this);

		HorizontalPanel hpnbtngroup = new HorizontalPanel();
		hpnbtngroup.add(btnaddgroup);
		hpnbtngroup.add(btnremovegroup);

		TitleBodyWidget wGroup = new TitleBodyWidget(constants.userGroupAssignable(), constants.userGroupAssignableTooltip(), lstusergroups, hpnbtngroup, (((MainApp.getBodyPanelWidth()-80) * 0.25)-17) + "px", "100%");


		// LANGUAGE LIST			
		lstuserlangs.setVisibleItemCount(8);
		lstuserlangs.setMultipleSelect(true);
		lstuserlangs.setHeight("100px");
		
		HorizontalPanel hpnbtnlang = new HorizontalPanel();
		hpnbtnlang.add(btnaddlang);
		hpnbtnlang.add(btnremovelang);  

		btnaddlang.setTitle(constants.buttonAdd());
		btnremovelang.setTitle(constants.buttonRemove());
		btnaddlang.addClickHandler(this);
		btnremovelang.addClickHandler(this);

		
		TitleBodyWidget wLang = new TitleBodyWidget(constants.userLangDeclared(), constants.userLangDeclaredTooltip(), lstuserlangs, hpnbtnlang, (((MainApp.getBodyPanelWidth()-80) * 0.25)-17)  + "px", "100%");

		// USER DETAIL
		
		ft.setWidget(0, 0, new HTML(constants.registerLoginName()));
		ft.setWidget(0, 1, txtloginname);
		txtloginname.setWidth("100%");
		txtloginname.setReadOnly(true);
		ft.getFlexCellFormatter().setColSpan(0, 1, 3);

		ft.setWidget(1, 0, new HTML(constants.registerFirstName()));
		ft.setWidget(1, 1, txtfname);
		txtfname.setWidth("100%");
		txtfname.setReadOnly(true);
		ft.setWidget(1, 2, new HTML(constants.registerLastName()));
		ft.setWidget(1, 3, txtlname);
		txtlname.setWidth("100%");
		txtlname.setReadOnly(true);

		ft.setWidget(2, 0, new HTML(constants.registerEmail()));
		ft.setWidget(2, 1, txtemail);
		txtemail.setWidth("100%");
		txtemail.setReadOnly(true);
		ft.setWidget(2, 2, new HTML(constants.registerAffiliation()));
		ft.setWidget(2, 3, txtaffiliation);
		txtaffiliation.setWidth("100%");
		txtaffiliation.setReadOnly(true);
				
		ft.setWidget(3, 0, new HTML(constants.registerContactAddress()));
		ft.setWidget(3, 1, txtaddress);
		txtaddress.setWidth("100%");
		txtaddress.setReadOnly(true);
		ft.getFlexCellFormatter().setColSpan(3, 1, 3);
		
		ft.setWidget(4, 0, new HTML(constants.registerCountry()));
		ft.setWidget(4, 1, txtcountry);
		txtcountry.setWidth("100%");
		txtcountry.setReadOnly(true);
		ft.setWidget(4, 2, new HTML(constants.registerPostalCode()));
		ft.setWidget(4, 3, txtpostcode);
		txtpostcode.setWidth("100%");
		txtpostcode.setReadOnly(true);

		ft.setWidget(5, 0, new HTML(constants.registerURL()));
		ft.setWidget(5, 1, txturl);
		txturl.setWidth("100%");
		txturl.setReadOnly(true);
		ft.setWidget(5, 2, new HTML(constants.registerChat()));	
		ft.setWidget(5, 3, txtchat);
		txtchat.setWidth("100%");
		txtchat.setReadOnly(true);
		
		ft.setWidget(6, 0, new HTML(constants.registerWorkPhone()));
		ft.setWidget(6, 1, txtwphone);
		txtwphone.setWidth("100%");
		txtwphone.setReadOnly(true);
		ft.setWidget(6, 2, new HTML(constants.registerMobilePhone()));
		ft.setWidget(6, 3, txtmphone);
		txtmphone.setWidth("100%");
		txtmphone.setReadOnly(true);
		
		ft.setWidget(7, 0, wGroup);
		ft.getFlexCellFormatter().setColSpan(7, 0, 2);
		ft.setWidget(7, 1, wLang);
		ft.getFlexCellFormatter().setColSpan(7, 1, 2);
		
		TitleBodyWidget wUserDetail = new TitleBodyWidget(constants.userDetails(), GridStyle.setTableRowStyle(ft, "#F4F4F4", "#E8E8E8", 4), null,  ((MainApp.getBodyPanelWidth()-80) * 0.5)  + "px", "100%");		
		
		userSubPanel.add(wUserDetail);
		
		VerticalPanel userMainPanel = new VerticalPanel();
		userMainPanel.setSize("500px", "100%");
		userMainPanel.add(userSubPanel);
		
		HorizontalPanel subPanel = new HorizontalPanel();
		subPanel.add(userMainPanel);					
		subPanel.setSpacing(10);
		subPanel.setCellHorizontalAlignment(userMainPanel,  HasHorizontalAlignment.ALIGN_LEFT);
		
		panel.clear();
		panel.add(userMainPanel);	      
		panel.setCellHorizontalAlignment(userMainPanel,  HasHorizontalAlignment.ALIGN_LEFT);
		panel.setCellVerticalAlignment(userMainPanel,  HasVerticalAlignment.ALIGN_TOP);

		initWidget(panel);
		initCountryList();
	}
	
	public void loadUserDetail(final String userid, String ontologyid){
		this.userid = userid;
		this.ontologyid = ontologyid;
		AsyncCallback<Users>callback = new AsyncCallback<Users>() {
			public void onSuccess(Users user) {
				userSubPanel.setVisible(true);
				txtloginname.setText(user.getUsername());
				txtfname.setText(user.getFirstName());
				txtlname.setText(user.getLastName());
				txtemail.setText(user.getEmail());
				txtaffiliation.setText(user.getAffiliation());
				txtaddress.setText(user.getContactAddress());
				txtpostcode.setText(user.getPostalCode());
				txtcountry.setText(countryList.get(user.getCountryCode()));
				txturl.setText(user.getCountryCode());
				txturl.setText(user.getUserUrl());
				txtwphone.setText(user.getWorkPhone());
				txtmphone.setText(user.getMobilePhone());
				txtchat.setText(user.getChatAddress());
				
				initGroupList(userid);
				initUserLang(userid);
			}

			public void onFailure(Throwable caught) {
				ExceptionManager.showException(caught, constants.userListUserFail());
			}
		};
		Service.systemService.getUser(userid, callback);
		
	}
	
	private void initCountryList(){
		AsyncCallback<ArrayList<String[]>>callback = new AsyncCallback<ArrayList<String[]>>() {
			public void onSuccess(ArrayList<String[]> tmp) {
				for(int i=0;i<tmp.size();i++){
					String[] item = (String[]) tmp.get(i); 
					countryList.put(item[1], item[0]);			    						    		
				}
			}

			public void onFailure(Throwable caught) {
				ExceptionManager.showException(caught, constants.userListUserFail());
			}
		};
		Service.systemService.getCountryCodes(callback);
		
	}

	public void generateListData(final Widget sender,String querystr,final String errmsg){

		AsyncCallback<ArrayList<String[]>>cbklist = new AsyncCallback<ArrayList<String[]>>() {
			public void onSuccess(ArrayList<String[]> tmp) {
				((ListBox) sender).clear();
				for(int i=0;i<tmp.size();i++){
					String[] item = (String[]) tmp.get(i); 
					((ListBox) sender).addItem(item[0],item[1]);	
				}
			}

			public void onFailure(Throwable caught) {
				ExceptionManager.showException(caught, errmsg);
			}
		};
		Service.queryService.execHibernateSQLQuery(querystr, cbklist);			
	}
	
	private void initGroupList(String userid) {
		lstusergroups.clear();

		String sqlStr = "SELECT DISTINCT users_groups_name,users_groups.users_groups_id " +
		" FROM users_groups INNER JOIN users_groups_map " +
		"ON users_groups.users_groups_id = users_groups_map.users_group_id " +
		" WHERE users_groups_map.users_id= '"+userid +"'";
		generateListData(lstusergroups, sqlStr, constants.userListGroupFail());
	}

	private void initUserLang(String userid) {
		lstuserlangs.clear();
		
		String sqlStr = "SELECT local_language,language_code FROM language_code WHERE " +
				 "language_code IN ( SELECT language_code FROM users_language WHERE user_id =  '"+userid +"' and status=1) order by language_order";
		
		generateListData(lstuserlangs, sqlStr, constants.userListLangFail());
	}   	
	
	public HashMap<String,String> getUserLang()
	{
		final HashMap<String,String> data = new HashMap<String,String>();
		for(int i=0;i<lstuserlangs.getItemCount(); i++){
			data.put(lstuserlangs.getItemText(i), lstuserlangs.getValue(i));
		}
		return data;
	}
	
	public HashMap<String,String> getUserGroup()
	{
		final HashMap<String,String> data = new HashMap<String,String>();
		for(int i=0;i<lstusergroups.getItemCount(); i++){
			data.put(lstusergroups.getItemText(i), lstusergroups.getValue(i));
		}
		return data;
	}

	@Override
	public void onClick(ClickEvent event) {
		Widget sender = (Widget) event.getSource();
		
		if(Window.confirm(constants.userLangGroupAlert()))
		{
		
			if (sender==btnaddlang){
				if(newUserDialog == null || !newUserDialog.isLoaded)
					newUserDialog = new UserAssignDialogBox(2, userid, ontologyid);				
				newUserDialog.show();
			}else if(sender == btnremovelang){	
				if((Window.confirm(constants.userConfirmRemoveLang()))==false){
					return;
				}
				if(lstuserlangs.getSelectedIndex()==-1){
					Window.alert(constants.userNoLang());
					return;
				}
				AsyncCallback<Integer> cbkdellang = new AsyncCallback<Integer>() {
					public void onSuccess(Integer result) {
						lstuserlangs.removeItem(lstuserlangs.getSelectedIndex());
						// create shell command for send mail to user in group 
					}
	
					public void onFailure(Throwable caught) {
						ExceptionManager.showException(caught, constants.userLangRemoveFail());
					}
				};
				/*String sql1 = "DELETE FROM users_language " +
				"WHERE language_code='"+lstuserlangs.getValue(lstuserlangs.getSelectedIndex())+"'" +
				"AND user_id ='"+lstusers.getValue(lstusers.getSelectedIndex())+"'";*/
				String sql1 = "DELETE FROM users_language_projects " +
						"WHERE language_code='"+lstuserlangs.getValue(lstuserlangs.getSelectedIndex())+"'" +
						"AND user_id ='"+userid+"' AND project_id ='"+ontologyid+"' ";
	
				Service.queryService.hibernateExecuteSQLUpdate(sql1,cbkdellang);
	
			}else if(sender == btnaddgroup){
				if(newUserDialog == null || !newUserDialog.isLoaded)									
					newUserDialog = new UserAssignDialogBox(1,userid, ontologyid);				
				newUserDialog.show();
			}else if(sender == btnremovegroup){
				if((Window.confirm(constants.userConfirmRemoveGroup()))==false){
					return;
				}
				if(lstusergroups.getSelectedIndex()==-1){
					Window.alert(constants.userNoGroup());
					return;
				}
				AsyncCallback<Integer> cbkdelgroup = new AsyncCallback<Integer>() {
					public void onSuccess(Integer result) {
						lstusergroups.removeItem(lstusergroups.getSelectedIndex());
						// create shell command for send mail to user in group 
					}
					public void onFailure(Throwable caught) {
						ExceptionManager.showException(caught, constants.userGroupRemoveFail());
					}
				};
				/*String sqlStr = "DELETE FROM users_groups_map " +
				"WHERE users_group_id='"+lstusergroups.getValue(lstusergroups.getSelectedIndex())+"'" +
				"AND users_id ='"+lstusers.getValue(lstusers.getSelectedIndex())+"'";*/
				String sqlStr = "DELETE FROM users_groups_projects " +
						"WHERE users_group_id='"+lstusergroups.getValue(lstusergroups.getSelectedIndex())+"'" +
						"AND users_id ='"+userid+"' AND project_id ='"+ontologyid+"' ";
				Service.queryService.hibernateExecuteSQLUpdate(sqlStr,cbkdelgroup);
			}	
		}
	}
	
	private class UserAssignDialogBox extends DialogBoxAOS implements ClickHandler{
		private VerticalPanel userpanel = new VerticalPanel();
		private Button btngroupadd = new Button(constants.buttonAdd());
		private Button btngroupcancel = new Button(constants.buttonCancel());
		private ListBox lstdata = new ListBox();
		private TextBox txthidden = new TextBox();
		private CheckBox showAll = new CheckBox(constants.userShowOnlyRequested(), true);
		
		private boolean chkExistingItem(ListBox listbox, String item)
		{
			for(int i=0;i<listbox.getItemCount(); i++){
				if(item.equals(listbox.getValue(i)))
					return true;
			}
			return false;
		}

		public void initData(int selecteduserid, int selectedprojectid, final int typeid, boolean isAll)
		{
			
			AsyncCallback<ArrayList<String[]>> callbackpref = new AsyncCallback<ArrayList<String[]>>() {
				public void onSuccess(ArrayList<String[]> user) {
					lstdata.clear();
					for(int i=0;i<user.size();i++){
			    		String[] item = (String[]) user.get(i);
			    		boolean chk = false;
			    		if(typeid ==1)
			    			chk = chkExistingItem(lstusergroups, item[1]);
						else if(typeid ==2)
							chk = chkExistingItem(lstuserlangs, item[1]);
			    		if(!chk)
			    			lstdata.addItem(item[0], item[1]);					    		
			    	}
				 }
		
			    public void onFailure(Throwable caught) {
			    	if(typeid ==1)
			    		ExceptionManager.showException(caught, constants.userListGroupFail());
					else if(typeid ==2)
						ExceptionManager.showException(caught, constants.userListLangFail());
			    }
			};
			
			switch (typeid) 
			{
				case 1 : // Add user group
					this.setText(constants.userSelectGroup());
					showAll.setVisible(true);
					if(isAll)
					{
						UserPreferenceServiceUtil.getInstance().getPendingGroup(selecteduserid, selectedprojectid, callbackpref);
					}
					else
					{
						UserPreferenceServiceUtil.getInstance().getNonAssignedAndPendingGroup(selecteduserid, selectedprojectid, callbackpref);
					}
					break;
				case 2 : // Add user language
					this.setText(constants.userSelectLang());
					showAll.setVisible(true);
					if(isAll)
					{
						UserPreferenceServiceUtil.getInstance().getPendingLanguage(selecteduserid, selectedprojectid, callbackpref);
					}
					else
					{
						UserPreferenceServiceUtil.getInstance().getNonAssignedAndPendingLanguage(selecteduserid, selectedprojectid, callbackpref);
					}
					break;
			}
		}
		
		public UserAssignDialogBox(final int typeid, final String selecteduserid, final String selectedprojectid) {
			
			initData(Integer.parseInt(selecteduserid), Integer.parseInt(selectedprojectid), typeid, false);
			
			final FlexTable table = new FlexTable();
			table.setBorderWidth(0);
			table.setCellPadding(0);
			table.setCellSpacing(1);
			table.setWidth("100%");
			table.setWidget(0, 0, new HTML(""));
			table.setWidget(1,0,lstdata);
			lstdata.setVisibleItemCount(20);
			lstdata.setMultipleSelect(true);
			lstdata.setSize("250px", "200px");

			table.getFlexCellFormatter().setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);
			table.getFlexCellFormatter().setHorizontalAlignment(1, 0, HasHorizontalAlignment.ALIGN_CENTER);

			// Popup element
			HorizontalPanel tableHP = new HorizontalPanel();
			//tableHP.setSpacing(10);
			tableHP.add(table);
			userpanel.add(tableHP);
			
			showAll.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					initData(Integer.parseInt(selecteduserid), Integer.parseInt(selectedprojectid), typeid, showAll.getValue());
				}
			});
			
			HorizontalPanel showAllPanel = new HorizontalPanel();
			showAllPanel.add(showAll);
			showAllPanel.setCellHorizontalAlignment(showAll, HasHorizontalAlignment.ALIGN_LEFT);
			
			HorizontalPanel hp = new HorizontalPanel();
			hp.setSize("100%", "100%");
			hp.add(showAllPanel);
			
			hp.add(btngroupadd);				
			btngroupadd.addClickHandler(this);
			hp.add(btngroupcancel);
			hp.setSpacing(5);
			hp.setCellWidth(showAllPanel, "100%");
			hp.setCellHorizontalAlignment(showAllPanel, HasHorizontalAlignment.ALIGN_LEFT);
			hp.setCellVerticalAlignment(showAllPanel, HasVerticalAlignment.ALIGN_MIDDLE);
			hp.setCellHorizontalAlignment(btngroupadd, HasHorizontalAlignment.ALIGN_RIGHT);
			hp.setCellHorizontalAlignment(btngroupcancel, HasHorizontalAlignment.ALIGN_RIGHT);
			
			VerticalPanel hpVP = new VerticalPanel();			
			hpVP.setStyleName("bottombar");
			hpVP.setSize("100%", "100%");
			hpVP.add(hp);
			hpVP.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);

			btngroupcancel.addClickHandler(this);
			txthidden.setText(Integer.toString(typeid));
			txthidden.setVisible(false);
			userpanel.add(hpVP);
			userpanel.add(txthidden);
			userpanel.setCellHorizontalAlignment(hp,HasHorizontalAlignment.ALIGN_CENTER);
			
			setWidget(userpanel);
		}

		public void onClick(ClickEvent event) {
			Widget sender = (Widget) event.getSource();
			if(sender.equals(btngroupcancel)){			
				this.hide();
			}else if(sender.equals(btngroupadd)){
				//---- Save process
				if(lstdata.getSelectedIndex()==-1){
					Window.alert(constants.userNoData());
					return;
				}
				
				if(lstdata.getItemCount()>0){
					switch (Integer.parseInt(txthidden.getText())){
					case 1: // groups
						for(int i=0;i<lstdata.getItemCount(); i++){
							if(lstdata.isItemSelected(i)){
								lstusergroups.addItem(lstdata.getItemText(i), lstdata.getValue(i));
							}
						}
						
						break;
					case 2: // languages
						for(int i=0;i<lstdata.getItemCount(); i++){
							if(lstdata.isItemSelected(i)){
								lstuserlangs.addItem(lstdata.getItemText(i), lstdata.getValue(i));
							}
						}
					}
				}
				
				this.hide();
			}
		} // onclick
	} //--- user Dialog box

}