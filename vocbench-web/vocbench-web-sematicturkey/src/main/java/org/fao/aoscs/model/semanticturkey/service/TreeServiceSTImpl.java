package org.fao.aoscs.model.semanticturkey.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.fao.aoscs.domain.OntologyInfo;
import org.fao.aoscs.domain.TreeObject;
import org.fao.aoscs.domain.TreePathObject;
import org.fao.aoscs.model.semanticturkey.service.manager.VocbenchManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rajbhandari
 *
 */
public class TreeServiceSTImpl {
	
	protected static Logger logger = LoggerFactory.getLogger(TreeServiceSTImpl.class);
	
	/**
	 * @param rootConceptURI
	 * @param schemeURI
	 * @param ontoInfo
	 * @param showAlsoNonpreferredTerms
	 * @param isHideDeprecated
	 * @param langList
	 * @return
	 */
	public ArrayList<TreeObject> getTreeObject(String rootConceptURI, String schemeURI, OntologyInfo ontoInfo, boolean showAlsoNonpreferredTerms, boolean isHideDeprecated, ArrayList<String> langList) {
		logger.debug("getTreeObject(" + rootConceptURI + ", " + isHideDeprecated + ", "+ langList + ")");

		ArrayList<TreeObject> treeObjList = new ArrayList<TreeObject>();
		final HashMap<String, TreeObject> cList = new HashMap<String, TreeObject>();
		final ArrayList<TreeObject> emptycList = new ArrayList<TreeObject>();
		
		ArrayList<TreeObject> treeObjs = new ArrayList<TreeObject>();
		
		if(rootConceptURI==null)
			treeObjs = VocbenchManager.getTopConcepts(ontoInfo, schemeURI, null, showAlsoNonpreferredTerms, isHideDeprecated,langList);
		else
			treeObjs = VocbenchManager.getNarrowerConcepts(ontoInfo, rootConceptURI, schemeURI, showAlsoNonpreferredTerms, isHideDeprecated,langList);
		
		if(treeObjs.size()>0){
			for(TreeObject treeObj : treeObjs) {
				if(treeObj!=null)
				{
					if(treeObj.getLabel().startsWith("###EMPTY###"))
						emptycList.add(treeObj);
					else
					{
						String label = treeObj.getLabel();
						label = label.replace("<b>", "").replace("</b>", "");
						cList.put(label+treeObj.getUri(), treeObj);
					}
				}
			}
			List<String> labelKeys = new ArrayList<String>(cList.keySet()); 
			Collections.sort(labelKeys, String.CASE_INSENSITIVE_ORDER);
			
			for (Iterator<String> itr = labelKeys.iterator(); itr.hasNext();){ 
    			treeObjList.add(cList.get(itr.next()));
            }
			for (Iterator<TreeObject> itr = emptycList.iterator(); itr.hasNext();){ 
    			treeObjList.add(itr.next());
            }
		}
	    return treeObjList;
	}
	
	/**
	 * @param conceptURI
	 * @param schemeURI
	 * @param ontoInfo
	 * @return
	 */
	public TreePathObject getTreePath(String conceptURI, String schemeURI, OntologyInfo ontoInfo){
		logger.debug("getTreePath(" + conceptURI + ")" );
		
		TreePathObject tpObj = new TreePathObject();
		if(conceptURI!=null){
			tpObj = VocbenchManager.getPathFromRoot(ontoInfo, conceptURI, schemeURI);
		}
		return tpObj;
	}
	
	
}
