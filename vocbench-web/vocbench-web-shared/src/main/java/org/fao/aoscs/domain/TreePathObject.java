package org.fao.aoscs.domain;

import java.util.ArrayList;

import net.sf.gilead.pojo.gwt.LightEntity;


public class TreePathObject extends LightEntity{

	private static final long serialVersionUID = 6484016629446705227L;

	private ArrayList<String> itemList = new ArrayList<String>();


	private String rootItem = new String();
	
	public ArrayList<String> getItemList() {
		return itemList;
	}
	public String getRootItem() {
		return rootItem;
	}
	public boolean isEmpty(){
		return itemList.isEmpty();
	}
	public void addItemList(String uri) {
		if(!this.itemList.contains(uri)){
			this.itemList.add(uri);
		}
	}
	public void setItemList(
			ArrayList<String> itemList) {
		this.itemList = itemList;
	}
	public void setRootItem(String rootItem) {
		this.rootItem = rootItem;
	}
}
