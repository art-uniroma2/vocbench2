package org.fao.aims.aos.vocbench.services;

import java.util.ArrayList;
import java.util.List;

public class ConceptForNarrower {
	private boolean isTopConcept;
	private List<String>narrowerConceptList;
	private String concept;
	private String show;
	
	public ConceptForNarrower(String concept, String show) {
		this.concept = concept;
		this.show = show;
		isTopConcept = true;
		narrowerConceptList = new ArrayList<String>();
	}
	
	public String getConcept(){
		return concept;
	}
	
	public String getShow(){
		return show;
	}

	public boolean isTopConcept() {
		return isTopConcept;
	}

	public void setTopConcept(boolean isTopConcept) {
		this.isTopConcept = isTopConcept;
	}

	public List<String> getNarrowerConceptList() {
		return narrowerConceptList;
	}
	
	public void addNarrowerConcept(String narrowerConcept){
		if(!narrowerConceptList.contains(narrowerConcept)){
			narrowerConceptList.add(narrowerConcept);
		}
	}
}
