package org.fao.aims.aos.vocbench.services;

public class XLabelInfo {
	private String xlabelURI = null;
	private boolean isPreferred = false;
	private String literal_label = null;
	private String literal_lang = null;
	private String created = null;
	private String modified = null;
	private String hasStatus = null;
	public String getXlabelURI() {
		return xlabelURI;
	}
	public void setXlabelURI(String xlabelURI) {
		this.xlabelURI = xlabelURI;
	}
	public boolean isPreferred() {
		return isPreferred;
	}
	public void setPreferred(boolean isPreferred) {
		this.isPreferred = isPreferred;
	}
	public String getLiteral_label() {
		return literal_label;
	}
	public void setLiteral_label(String literal_label) {
		this.literal_label = literal_label;
	}
	public String getLiteral_lang() {
		return literal_lang;
	}
	public void setLiteral_lang(String literal_lang) {
		this.literal_lang = literal_lang;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public String getHasStatus() {
		return hasStatus;
	}
	public void setHasStatus(String hasStatus) {
		this.hasStatus = hasStatus;
	}
	
	

}
