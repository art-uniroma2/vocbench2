package org.fao.aims.aos.vocbench.services;

import it.uniroma2.art.owlart.model.ARTURIResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConceptInfo {

	private String concept;
	private ARTURIResource conceptURI;
	//private Map <String, Map<String, String>> xLabelUriToMapXLabelInfoMap;
	private Map <String, XLabelInfo> xLabelUriToXLabelInfoMap;
	private List <ARTURIResource> broaderList;
	private boolean isTopConcept;
	private List<String> schemeList;
	private String created;
	private String modified;
	private String status;
	private boolean hasChild;
	
	public ConceptInfo() {
		conceptURI = null;
		xLabelUriToXLabelInfoMap = new HashMap<String, XLabelInfo>();
		broaderList = new ArrayList<ARTURIResource>();
		isTopConcept = false;
		schemeList = new ArrayList<String>();
		created = null;
		modified = null;
		status = null;
		hasChild = false;
	}

	public String getConcept() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	public ARTURIResource getConceptURI() {
		return conceptURI;
	}

	public void setConceptURI(ARTURIResource conceptURI) {
		this.conceptURI = conceptURI;
	}

	public Map<String, XLabelInfo> getxLabelUriToXLabelInfoMap() {
		return xLabelUriToXLabelInfoMap;
	}

	public void setxLabelUriToMapXLabelInfoMap(Map<String, XLabelInfo> xLabelUriToMapXLabelInfoMap) {
		this.xLabelUriToXLabelInfoMap = xLabelUriToMapXLabelInfoMap;
	}

	public List<ARTURIResource> getBroaderList() {
		return broaderList;
	}

	public void addBroader(ARTURIResource broader) {
		if(!broaderList.contains(broader)){
			broaderList.add(broader);
		}
	}

	public boolean isTopConcept() {
		return isTopConcept;
	}

	public void setTopConcept(boolean isTopConcept) {
		this.isTopConcept = isTopConcept;
	}

	public List<String> getSchemeList() {
		return schemeList;
	}

	public void addScheme(String scheme) {
		if(!schemeList.contains(scheme)){
			schemeList.add(scheme);
		}
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean hasChild() {
		return hasChild;
	}

	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}
	
	
}
